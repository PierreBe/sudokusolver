'use strict'
document.addEventListener('DOMContentLoaded', main)

let inputDiv // opt
let outputDiv // opt
let resolveButton // opt

function main() {
    document.removeEventListener('DOMContentLoaded', main)
    inputDiv = document.getElementById('input') // opt
    outputDiv = document.getElementById('output') // opt
    resolveButton = document.getElementById('resolve-button') // opt
    let input = document.createElement('input')
    for (let i = 0; i < 81; i++) {
        inputDiv.appendChild(input.cloneNode())
    }
    inputDiv.addEventListener('keydown', keyPressed)
    // inputDiv.addEventListener('input', handleInput)
    resolveButton.addEventListener('click', handleClick)

    inputDiv.addEventListener('mouseover', handleHover)
    inputDiv.addEventListener('mouseout', handleHover)

    inputDiv.firstElementChild.focus()
}

function keyPressed(event) { // ! temp, TODO : remove switch and merge keyPressed and handleInput
    event.preventDefault()
    let v
    switch(event.keyCode) {
        case 37: // ←
            if (event.target.previousElementSibling)
                event.target.previousElementSibling.focus()
            break
        case 38: // ↑
            v = Array.from(event.target.parentElement.children)
            v = v[v.indexOf(event.target) - 9]
            if (v)
                v.focus()
            break
        case 40: // ↓
            v = Array.from(event.target.parentElement.children)
            v = v[v.indexOf(event.target) + 9]
            if (v)
                v.focus()
            break

        case 8: // backspace
            if (event.target.previousElementSibling)
                    event.target.previousElementSibling.focus()
        case 46: // delete
            event.target.value = ''
            // event.data = ''
            handleInput(event)
            break
        
        case 82: // r // ! temp, fill sudoku :
            let sudoku = '000600040740583006910720800001007590003106200078200100004072081100368059030001000'
            for (let i = 0; i < 81; i++) {
                inputDiv.children[i].value = sudoku.charAt(i) == 0 ? '' : sudoku.charAt(i)
            }
            resolveButton.focus()
            break
        
        case 65: // a
            v = Math.floor(1 + Math.random() * 18)
            keyPressed({
                target: event.target,
                key: v,
                keyCode: 96 + v,
                currentTarget: event.currentTarget,
                preventDefault(){}
            })
            break

        case 97: // 1
        case 98:
        case 99:
        case 100:
        case 101:
        case 102:
        case 103:
        case 104:
        case 105: // 9
        case 49: // 1
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57: // 9
            event.target.value = event.key
            // event.data = event.key
            handleInput(event)
        case 39: // →
        default:
            if (event.target.nextElementSibling)
                event.target.nextElementSibling.focus()
            else
                resolveButton.focus()
            break
    }
}

let redundantPairs = Array() // new Set() // add, size, delete
function handleInput(event) {
    // event.target.value = isNumeric(event.data) ? event.data : ''

    // check validity :
    for (let i = 0; i < redundantPairs.length; i++) {
        for (let cell of redundantPairs[i]) {
            if (cell == event.target) {
                for (let cell of redundantPairs[i]) cell.style.backgroundColor = cell.style.previousBackgroundColor
                redundantPairs.splice(i--, 1)
                break
            }
        }
    }
    if (event.target.value != '') {
        let arr = Array.from(event.currentTarget.children)
        for(let i of getRelated(arr, arr.indexOf(event.target)))
            if (i.value == event.target.value)
                redundantPairs.push([i, event.target])
    }
    for (let pair of redundantPairs) {
        for (let cell of pair) {
            if (cell.style.backgroundColor != 'red') {
                cell.style.previousBackgroundColor = cell.style.backgroundColor
                cell.style.backgroundColor = 'red'
            }
        }
    }

    // if (event.target.nextSibling) event.target.nextSibling.focus()
    // else resolveButton.focus()
}

function handleClick() {
    outputDiv.innerHTML = ''
    if(redundantPairs.length) { // invalid grid
        outputDiv.textContent = 'La grille comporte des erreurs. Corrigez-les pour pouvoir lancer la résolution.'
    } else { // valid grid
        // create the grid :
        let gridDiv = document.createElement('div')
        gridDiv.className = 'grid'
        let cellDiv
        for (let c of inputDiv.children) {
            cellDiv = document.createElement('div')
            if (c.value != '') {
                cellDiv.textContent = c.value
                cellDiv.style.backgroundColor = 'rgb(31,31,31)'
            } else {
                cellDiv.textContent = 0
            }
            gridDiv.appendChild(cellDiv)
        }
        // gridDiv.addEventListener('mouseover', handleHover)
        // gridDiv.addEventListener('mouseout', handleHover)
        outputDiv.appendChild(gridDiv)
        // solve :
        solve(gridDiv)
        let plural = outputDiv.children.length > 2 ? 's' : ''
        gridDiv.textContent = outputDiv.children.length - 1 + ` solution${plural} trouvée${plural} !`
    }
}

const DARK_GREY = 'rgb(63, 63, 63)'
const DARKER_GREY = 'rgb(31, 31, 31)'
function handleHover(event) {
    if(event.target != event.currentTarget) {
        let bgc1 = '', bgc2 = ''
        if (event.type == 'mouseover') {
            bgc1 = DARKER_GREY
            bgc2 = DARK_GREY
        }/*  else if (event.type == 'mouseout') {
            bgc1 = ''
            bgc2 = ''
        } */
        let arr = Array.from(event.currentTarget.children)
        for(let i of getRelated(arr, arr.indexOf(event.target)))
            if (i.style.backgroundColor == '' || i.style.backgroundColor == DARKER_GREY) i.style.backgroundColor = bgc1
            else i.style.previousBackgroundColor = bgc1
        if (event.target.style.backgroundColor == '' || event.target.style.backgroundColor == DARKER_GREY || event.target.style.backgroundColor == DARK_GREY) event.target.style.backgroundColor = bgc2
        else event.target.style.previousBackgroundColor = bgc2
    }
}

function solve(gridDiv, index=0) {
    if (index < 81) {
        if (gridDiv.children[index].textContent == 0) {
            let arr = Array.from(gridDiv.children)
            let notIn
            for(let i of '123456789') {
                notIn = true
                for (let j of getRelated(arr, index)) {
                    if (i == j.textContent) {
                        notIn = false
                        break
                    }
                }
                if (notIn) {
                    gridDiv.children[index].textContent = i
                    solve(gridDiv, index+1)
                }
                gridDiv.children[index].textContent = 0
            }
        } else {
            solve(gridDiv, index+1)
        }
    } else {
        gridDiv.parentElement.appendChild(gridDiv.cloneNode(true))
    }
}

function getRelated(arr, index) {
    return Array.from(new Set([].concat(
        arr.slice(Math.floor(index/9)*9,Math.floor(index/9)*9+9),
        arr.filter((v, i) => i % 9 == index % 9),
        arr.filter((v, i) => i > Math.floor(index/27)*27-1 && i < Math.floor(index/27)*27+27 && i % 9 < Math.floor(index%9/3)*3+3 && i % 9 > Math.floor(index%9/3)*3-1)
    ))).filter((v, i) => i != index % 9)
}

function isNumeric(char) {
    return '123456789'.includes(char)
}